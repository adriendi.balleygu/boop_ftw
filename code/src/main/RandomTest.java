package main;

import java.util.ArrayList;
import java.util.Random;

public class RandomTest{
    //static Random random = new Random();
    static int crtDay = 0;
    static ArrayList<Book> selectBooks = new ArrayList<>();

    public static int[] randomSelect(int days, Library[] libraries){
        ArrayList<Integer> ret=new ArrayList<>();
        for(int i = 0; i < libraries.length; i++){
            if(crtDay >= days){
                break;
            }
            else if(libraries[i].d + crtDay >= days){
                continue;
            }
            ret.add(i);
            //int d=libraries[i].n/libraries[i].s+1;
            crtDay += libraries[i].d;
            //int[] cal=new int[d];
            for(int j = crtDay; j < days; j++){
                if(libraries[i].noMoreBook()) break;
                libraries[i].shipBook(j-crtDay);
                /*int r;
                while(cal[(r=random.nextInt(d))]==1);
                cal[r]=1;
                libraries[i].shipBook(r);*/
            }
        }
        int[] r=new int[ret.size()];
        for(int i=0;i<r.length;i++){
            r[i]=ret.get(i).intValue();
        }
        return r;
    }
}
