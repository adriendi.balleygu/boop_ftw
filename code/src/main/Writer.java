package main;

import java.io.FileWriter;
/*
Purpose of this class is to write in a file element by element
Elements can be String, int, double or character
As is ahs been made for problem solving output, it's advised to use it to write repetitive
model of words in your file/command line

PLEASE BE AWARE : this class is not idiot proof, so don't tempt the devil be doing foolish things
this being said, it takes the element one by one.
When you're satisfied with your lines, you can use the toFile(path to the file) or
toTerm to write it to a file or to the terminal

The separator can be indicated in the constructor (by default, it will be a space).
Put the elements by selecting the type (addInt to add an int to the file)
each add will be followed by the separator. To make a new line, call the newLine() method
 */

public class Writer{
    private StringBuilder str;
    private String cut;
    /*
    take the separator as the unique parameter
     */
    public Writer(String c){
        cut=c;
        str=new StringBuilder();
    }
    /*
    takes no parameter. The separator will be a " "
     */
    public Writer(){
        this(" ");
    }
    /*
    to add a String to the final output
     */
    public void addStr(String s){
        str.append(s+cut);
    }
    /*
    to add an int to the final output
     */
    public void addInt(int i){
        str.append(i+cut);
    }
    /*
    to add a double to the final output
     */
    public void addDbl(double d){
        str.append(d+cut);
    }
    /*
    to add a char to the final output
     */
    public void addChar(char c){
        str.append(c+cut);
    }
    /*
    to make a new line in the final output
     */
    public void newLine(){
        str.append("\n");
    }
    /*
    to write the final output in a file
    The parameter is the absolute path of the output file
     */
    public void toFile(String path){
        try{
            FileWriter fw=new FileWriter(path);
            fw.write(str.toString());
            fw.close();
        }catch(Exception e){
            System.out.println("couldn't write in the file");
            e.printStackTrace();
        }
    }
    /*
    to write the final output in the terminal
     */
    public void toTerm(){
        System.out.println(str.toString());
    }
}
