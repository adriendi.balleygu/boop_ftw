package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
/*
Purpose of this class is to read element by element from a file
Elements can be String, int, double or character.
As it has been made for problem solving input, it's advised to use it when you have a repetitive
model of words in your file.

PLEASE BE AWARE : this class is not idiot proof, so don't tempt the devil by doing foolish things
this being said, it take first (in the constructor) the absolute path from the file you want to
read with the splitter you want to use (by default a single space character)

Collect your elements by asking for them one by one (nxStr() if you want a string for example).
Call the hasMore() method at the end of each line from the input file
 */


public class Reader{
    private BufferedReader r;
    private String cut;
    private String[] line;
    private int i;
    /*
    takes the absolute path of the input file as first parameter
          the splitter you want to use as second parameter
    */
    public Reader(File src,String c){
        try{
            i=0;
            cut=c;
            r=new BufferedReader(new FileReader(src));
            line=r.readLine().split(cut);
        }catch(Exception e){
            System.out.println("Problem to open the file detected");
            e.printStackTrace();
        }
    }
    /*
    Take the input file you want to use as the unique parameter
    The splitter will be " " in this case
     */
    public Reader(File src){
        this(src," ");
    }
    /*
    Call this method to know if you get to the end of the file
    Please don't forget to call it at the end of each line from your input
    as it's your only way to get the next line

    When it detect the end os the file, it close the reader since he wont
    in any way useful afterward
     */
    public boolean hasMore(){
        try{
            String tmp=null;
            if(i<line.length ||(tmp=r.readLine())!=null){
                if(tmp!=null){
                    i=0;
                    line=tmp.split(cut);
                }
                return true;
            }
            r.close();
        }catch(Exception e){
            System.out.println("Can't read next line or close the reader");
            e.printStackTrace();
        }
        return false;
    }
    /*
    Return the next element in your file as a string
    */
    public String nxStr(){
        return line[i++];
    }
    /*
    Return the next element in your file as an int
    Please don't use this method if this element is not an int
     */
    public int nxInt(){
        return Integer.parseInt(line[i++]);
    }
    /*
    Return the next element in your file as a double
    Please don't use this method if this element is not a double
     */
    public double nxDbl(){
        return Double.parseDouble(line[i++]);
    }
    /*
    Return the first letter from the next element as a char
    Please don't use this method if this element is not a char
     */
    public char nxChar(){
        return line[i++].charAt(0);
    }
}
