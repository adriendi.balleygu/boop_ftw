package main;


import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class Boop{
    static int nbFileTry=4;
    public static void main(String[] args){
        boop();
    }
    public static void boop(){
        File[] f={  new File("D:\\ecoleIng\\HashCode\\2020\\code\\a_example.txt"),
                    new File("D:\\ecoleIng\\HashCode\\2020\\code\\b_read_on.txt"),
                    new File("D:\\ecoleIng\\HashCode\\2020\\code\\c_incunabula.txt"),
                    new File("D:\\ecoleIng\\HashCode\\2020\\code\\d_tough_choices.txt"),
                    new File("D:\\ecoleIng\\HashCode\\2020\\code\\e_so_many_books.txt"),
                    new File("D:\\ecoleIng\\HashCode\\2020\\code\\f_libraries_of_the_world.txt")};
        Reader r=new Reader(f[nbFileTry]);
        int nbBook,nbLibr,nbDays;
        nbBook=r.nxInt();
        nbLibr=r.nxInt();
        nbDays=r.nxInt();
        Book[] b=new Book[nbBook];
        Library[] l=new Library[nbLibr];
        r.hasMore();
        for(int i=0;i<nbBook;i++){
            b[i]=new Book(i,r.nxInt());
        }
        r.hasMore();
        for(int i=0;i<nbLibr;i++){
            int n;
            l[i]=new Library((n=r.nxInt()),r.nxInt(),r.nxInt(),i);
            r.hasMore();
            Book[] tmp=new Book[n];
            for(int j=0;j<n;j++){
                tmp[j]=b[r.nxInt()];
            }
            l[i].books=tmp;
            l[i].sortBook();
            //l[i].preCount();
            r.hasMore();
        }
        Arrays.sort(l,new Comparator<Library>(){
            public int compare(Library l1,Library l2){
                int tmp;
                tmp=l2.n-l1.n;
                if(tmp!=0) return tmp;
                tmp=l2.s-l1.s;
                if(tmp!=0) return tmp;
                tmp=l2.preScore-l1.preScore;
                if(tmp!=0) return tmp;
                return l1.d-l2.d;
            }
        });
        int[] selLib=RandomTest.randomSelect(nbDays,l);
        Writer w=new Writer();
        w.addInt(selLib.length);
        w.newLine();
        for(int i=0;i<selLib.length;i++){
            if(l[selLib[i]].i==0) continue;
            w.addInt(l[selLib[i]].id);
            w.addInt(l[selLib[i]].i);
            w.newLine();
            for(int j=0;j<l[selLib[i]].i;j++){
                w.addInt(l[selLib[i]].books[j].id);
            }
            w.newLine();
        }
        w.toFile("D:\\ecoleIng\\HashCode\\2020\\code\\"+(char)(nbFileTry+97)+"_out.txt");
    }
}
//D:\ecoleIng\HashCode\2020\code
