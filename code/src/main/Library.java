package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Library{
    public int n;
    public int d;
    public int s;
    public int id;
    public int i=0;
    public int score=0;
    public ArrayList<Book> books=new ArrayList<>();
    public Library(int n, int d, int s,int id){
        this.n = n;
        this.d = d;
        this.s = s;
        this.id=id;
    }
    public boolean noMoreBook(){
        return i>=books.size();
    }
    public Book[] shipBook(){
        ArrayList<Book> list=new ArrayList<>();
        for(int j=0;j<s;j++){
            list.add(books.get(i++));
        }
        Book[] ret=new Book[list.size()];
        return list.toArray(ret);
    }
    public void sortBook(){
        Collections.sort(books,new Comparator<Book>(){
            public int compare(Book b1,Book b2){
                return b1.score-b2.score;
            }
        });
    }
    public void avgScore(int daysLeft){
        daysLeft-=d;
        int lim=daysLeft<n/s?daysLeft:n/s;
        score=0;
        for(int i=0;i<lim*s;i++){
            if(i==books.size()) break;
            score+=books.get(i).score;
        }
        score/=(daysLeft+d);
    }
    public void remove(Book[] bId){
        for(int j=0;j<bId.length;j++){
            if(books.contains(bId[j])) books.remove(bId[j]);
        }
    }
}
